package com.test.client;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.List;


public class SplashActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                final ServerConnection serverConnection = ServerConnection.getInstance();
                try {
                    serverConnection.getOut().writeUTF("regions");
                    final String regions = serverConnection.getIn().readUTF();
                    Log.d("REGIONS", regions);
                    List<Region> regionsList = new Gson().fromJson(regions, new TypeToken<List<Region>>(){}.getType());
                    Log.d("REGIONS", Integer.toString(regionsList.size()));
                    Regions.setRegions(regionsList);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        this.startActivity(new Intent(this, MainActivity.class));
        this.finish();
    }

}
