package com.test.client;

import java.util.List;

public class Regions {

    private Regions() {}

    private static List<Region> regions;

    public static List<Region> getRegions() {
        return regions;
    }

    public static void setRegions(List<Region> regions) {
        Regions.regions = regions;
    }
}
