package com.test.client;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class RegionSelectableListActivity extends ActionBarActivity {

    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_region_selectable_lisst);

        listView = (ListView)this.findViewById(R.id.region_list);
        listView.setItemsCanFocus(false);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setAdapter(new RegionsSelectableAdapter(this, android.R.layout.simple_list_item_multiple_choice, Regions.getRegions()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_region_selectable_lisst, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_send:

                if(this.listView != null) {
                    final SparseBooleanArray selectedItems = this.listView.getCheckedItemPositions();
                    final int size = selectedItems.size();
                    final List<Region> selectedRegions = new ArrayList<>();
                    for(int i = 0; i < size; i++) {
                        final int key = selectedItems.keyAt(i);
                        final boolean isSelected = selectedItems.get(key);
                        if(isSelected) {
                            selectedRegions.add((Region)listView.getItemAtPosition(key));
                        }
                    }
                    final String json = new Gson().toJson(selectedRegions);
                    Toast.makeText(RegionSelectableListActivity.this, json, Toast.LENGTH_SHORT).show();

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            final ServerConnection serverConnection = ServerConnection.getInstance();
                            try {
                                serverConnection.getOut().writeUTF(json);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                }

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
