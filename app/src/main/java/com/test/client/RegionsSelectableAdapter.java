package com.test.client;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

class RegionsSelectableAdapter extends ArrayAdapter<Region> {

    RegionsSelectableAdapter(Context context, int resource, List<Region> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if(convertView == null) {
            view = View.inflate(this.getContext(), android.R.layout.simple_list_item_multiple_choice, null);
        }
        else {
            view = convertView;
        }

        final Region region = getItem(position);
        ((TextView)view).setText(region.getName());
        return view;
    }
}
