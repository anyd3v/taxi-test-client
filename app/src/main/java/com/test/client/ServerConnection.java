package com.test.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class ServerConnection {

    private static final String IP = "192.168.1.106";
    private static final int PORT = 6666;

    private static boolean isInit = false;
    private static ServerConnection instance;

    DataInputStream in;
    DataOutputStream out;

    public static void init() {
        try {
            final InetAddress ipAddress = InetAddress.getByName(IP);
            final Socket socket = new Socket(ipAddress, PORT);

            final InputStream sin = socket.getInputStream();
            final OutputStream sout = socket.getOutputStream();

            final DataInputStream in = new DataInputStream(sin);
            final DataOutputStream out = new DataOutputStream(sout);

            instance = new ServerConnection(in, out);
            isInit = true;

        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    public synchronized static ServerConnection getInstance() {
        if(!isInit) {
            init();
        }
        return instance;
    }

    private ServerConnection(final DataInputStream in, final DataOutputStream out) {
        this.in = in;
        this.out = out;
    }

    public DataInputStream getIn() {
        return this.in;
    }

    public DataOutputStream getOut() {
        return this.out;
    }
}
