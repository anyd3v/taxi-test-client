package com.test.client;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.ListView;

public class RegionListActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_region_list);

        final ListView listView = (ListView)this.findViewById(R.id.region_list);
        listView.setAdapter(new RegionsAdapter(this, android.R.layout.simple_list_item_1, Regions.getRegions()));
    }

}
